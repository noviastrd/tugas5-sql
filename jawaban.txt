1. Membuat Database
create database myshop;

2. Membuat Table di Dalam Database
a. Tabel users
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

b. Tabel categories
CREATE TABLE categories(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255)
);


c. Tabel items
CREATE TABLE items (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    description varchar(255),
    price INT,
    stock INT,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);


3. Memasukkan Data pada Table
a. Inser Tabel user
INSERT INTO users VALUES ('','John Doe', 'john@doe.com','john123'),
('','Jane Doe', 'jane@doe.com','jenita123');

b. Insert Tabel categories
INSERT INTO categories VALUES ('','gadget'), ('','cloth'), ('','men'), ('','women'), ('','branded');

c. Inser Tabel items
INSERT INTO items VALUES 
	('','sumsang b50','hape keren dari merek sumsang',4000000,100,1),
	('','Uniklooh','baju keren dari brand ternama',500000,50,2),
    	('','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. Mengambil Data dari Database
a. Mengambil data users
SELECT id, name, email FROM users;

b. Mengambil data items
-harga diatas 1000000
SELECT * FROM items WHERE price > 1000000 

-yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”
SELECT * FROM items where name LIKE '%uniklo%';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as 'kategori'
FROM items
INNER JOIN categories ON items.category_id=categories.id;

5. Mengubah Data dari Database
UPDATE items SET  price=2500000
WHERE name like '%sumsang%';

